# Configuration for my development environment

Clone the repository and run stow to link the config files

```bash
git clone git@gitlab.com:erickueen/dotfiles.git
cd dotfiles
stow .
```

## applications and packages:
  - [Ghostty](https://github.com/ghostty-org/ghostty) (terminal emulator)
  - [Zellij](https://github.com/zellij-org/zellij) (multiplexer)
  - [Yabai](https://github.com/koekeishiya/yabai) (tiling manager on mac)
  - [skhd](https://github.com/koekeishiya/skhd) (hotkey daemon for yabai)
  - [neovim](https://neovim.io/) (text editor)
  - [lazygit](https://github.com/jesseduffield/lazygit) (tui for git)

  ## Fonts

  Install Fira Code font.

  - MacOs:

  ```bash
  brew tap homebrew/cask-fonts
  brew install --cask font-fira-code
  ```

  - Linux (Ubuntu based):

  ```bash
  sudo apt install fonts-firacode
  ```
