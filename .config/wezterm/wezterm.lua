-- Pull in the wezterm API
local wezterm = require("wezterm")
local mux = wezterm.mux

-- This table will hold the configuration
local config = {}

-- Use the configu builder
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- Changing the color scheme:
config.color_scheme = "Catppuccin Macchiato"

-- Change font
config.font = wezterm.font("Fira Code")

-- Background opacity
config.window_background_opacity = 0.9

-- Hide tab bar when only one tab active
config.hide_tab_bar_if_only_one_tab = true

-- Hide title bar
config.window_decorations = "RESIZE"

config.window_frame = {
	border_left_width = "0cell",
	border_right_width = "0cell",
	border_bottom_height = "0cell",
	border_top_height = "0cell",
	border_left_color = "purple",
	border_right_color = "purple",
	border_bottom_color = "purple",
	border_top_color = "purple",
}

-- Font size
config.font_size = 18

-- prefer_egl=true
config.prefer_egl = true

config.window_padding = {
	left = "0px",
	right = "1cell",
	top = "0px",
	bottom = "0cell",
}

-- Maximize by default on startup
wezterm.on("gui-startup", function(cmd)
	local tab, pane, window = mux.spawn_window(cmd or {})
	window:gui_window():maximize()
end)
return config
