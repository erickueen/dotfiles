#!/bin/bash

# Mac specific setup
if [[ "$OSTYPE" = "darwin"* ]]; then
  # Install homebrew
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  if test ! $(which brew); then
    printf '\nHomebrew not found. Installing Homebrew...\n'
  fi

  # Homebrew casks
  CASKS=(
    emacs
    wezterm
    font-fira-code-nerd-font
    font-fira-code
  )

  # Homebrew packages
  PACKAGES=(
    asdf
    ghostty
    stow
    zellij
    lazygit
    wget
    zoxide
    fzf
    tmux
    coreutils
    ripgrep
    fd
    neovim
    zellij
    kubectl
    derailed/k9s/k9s
    kubectx
    vterm
    libvterm
    cmake
    ncurses
    coreutils
    postgresql
    htop
    fop
    libxslt
    wxwidgets
    unixodbc
    cloudflare/cloudflare/cloudflared
    ncurses
    gphoto2
    nicotine-plus
    koekeishiya/formulae/yabai
    koekeishiya/formulae/skhd
    openmtp
  )

  brew update
  brew install ${PACKAGES[@]}
  brew cleanup

  brew install cask

  brew install --cask ${CASKS[@]}
fi

# Setup oh my zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Install asdf plugins
asdf plugin add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
